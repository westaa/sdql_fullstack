https://aaw-sdql-fullstack.herokuapp.com/#/

Trend-based analytics are a double edged sword.

Almost every trend in every sport will regress to .500 over a long enough time period. So, isolating a trend which is currently winning at 60% against the spread can mean that it is in fact time to bet AGAINST this trend. However, if you have reason to believe that the trend is winning at 60% because oddsmakers are intentionally inflating lines to take advantage of a public bias (i.e. recreational bettors in the United States tend to bet overwhelmingly on teams who are heavily favored) then you may want to continue backing the trend.

Lines-makers intentionally put out bad lines in some sports and this is evident when running macro-queries on college basketball games and discovering that certain trends are covering spreads at above 55% in the recent past. Knowing that recreational bettors in the United States prefer high-scoring games and will thus overwhelmingly bet the over, oddsmakers inflate over-under spreads to take advantage of this bias.

This website and trend-based analytics should be just one piece of the pie in determining your own point spreads. It is designed to give you a very rough line which you can use in making your own handicap. The queries being run when a user selects "analyze" are based on micro-data: how each particular team in the matchup have performed with similar amounts of rest and similar points spreads in the past. And selecting "analyze" also runs queries to capture macro data: how all teams have performed in similar situations over the last two years. Where you will see the biggest difference between my point spreads and the Vegas line is where there is the largest confluence of micro and macro trend-based data supporting my line, and this may be a spot where you will want to continue analyzing the game and perhaps make a wager.

Key Numbers
When running analysis on point spreads, data will be returned in each sport based on how teams have performed with point spreads between key numbers in the same range and how all teams have performed with point spreads between key numbers in the same range.

USER STORIES TO BE COMPLETED

PERFORMANCE/CODE IMPROVEMENTS
- CODE to be moved to sdqlService
  - create function that takes array and returns average with decimal rounded two places

UX IMPROVMENTS

- check all queries against SDQL.com to make sure correct data displayed
- explain meaning of red and green and 'no data'
- write Angular filter that displays colleges actual names instead of the SDQL abbreviation

Macro Data
- How favorites and underdogs with point spread in similar range have performed over last two seasons
- How teams with similar rest have performed over the last two season
- How teams with similar rest AND similar point spreads have performed over the last two season
- Use "group by" queries to identify trends that correlate with other trends
  - i.e. How teams during this specific month have performed over the last year versus last five years, versus last ten years
- Chain promise to analyzeCBBdata function that, when resolved successfully, checks for trends in the immediate performance of both teams (previous five games over/under?, covered the spread previous three games?) and then runs queries to see how these trends hold up over last 1, 5, 10 years(run group-by query)

Standard Deviation
- most basketball teams give up when down 20 points or more with fewer than 5 minutes - where teams are covering point spreads by 20-30 points this should be taken into consideration and outliers impact reduced

On What Micro Data to Consider
- if data, Head to head matchups over last two years
- if teams are scoring an average of 6.5 points more or 6.5 points fewer from the 2015-2016 season to the 2016-2017 season, data on the 2015-2016 season SHOULD NOT be considered
  - write an IF statement that only gathers 2015-2016 data if point totals are within an average range of 6.5 points of the average point total in the 2016-2017 season
