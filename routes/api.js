var express = require('express');
var app = express();
var router = express.Router();
var unirest = require('unirest');
var rp = require('request-promise');
var day = new Date().toString().split(' ')[2];
var day2 = parseInt(day) + 1;
var monthText = new Date().toString().split(' ')[1];
var monthArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var numberMonthArr = ['01','02','03','04','05','06','07','08','09','10','11','12'];
var month = [];

function monthFinder () {
  for (var i = 0; i < monthArr.length; i++) {
    if (monthArr[i] == monthText) {
      month.push(numberMonthArr[i]);
    }
  }
}

router.get('/cbb', function (req, res, next) {
  monthFinder();
  console.log(month[0] + day);
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%40date%3D20170313' +
  // month[0] + day +
  '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y);
  })
});

router.get('/cbb/teamData/:team', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%2Cdate%40season%3E2015' +
  '%20and%20date%3C2017' + month[0] + day +
  '%20and%20team%3D' + req.params.team +
  '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y)
  })
})

router.get('/cbb/lastYearTeamData/:team', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%2Cdate%40season%3D2015' +
  '%20and%20team%3D' + req.params.team +
  '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y)
  })
});


router.get('/cbb/lineData/:line/:line2/:team/:year',function(req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%2Cdate%40season%3E' + req.params.year +
  '%20and%20date%3C2017' + month[0] + day +
  '%20and%20team%3D' + req.params.team + '%20and%20line%3E' + req.params.line + '%20and%20line%3C' + req.params.line2 + '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    console.log(y);
    res.send(y)
  }).catch(function(error){
    console.log(error);
  })
});

router.get('/cbb/restData/:team/:rest1/:rest2/:year', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%2Cdate%40season%3E'
  + req.params.year + '%20and%20date%3C2017' + month[0] + day +
  '%20and%20team%3D' + req.params.team +
  '%20and%20rest%3E%3D'  + req.params.rest1 + '%20and%20rest%3C' + req.params.rest2 + '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y)
  }).catch(function(error){
    console.log(error);
  })
})

router.get('/cbb/versusData/:team/:oTeam', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaabb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%2Cdate%40season%3E2014' + '%20and%20date%3C2017' + month[0] + day + '%20and%20team%3D' + req.params.team + '%20and%20o%3Ateam%3D' + req.params.oTeam +  '&output=json&api_key=guest').then(function(data) {
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y)
  }).catch(function(error) {
    console.log(error);
  })
})
// date,points,o:points@team=Bears and season=2011 and site
// date%2Cpoints%2Co%3Apoints%40team%3DBears%20and%20season%3D2011%20and%20site

router.get('/nba', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/nba/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%40date%3D2017' +
  month[0] + day +
  '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y);
  })
})

router.get('/nfl', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/nfl/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%40date%3E20170125' + '&output=json&api_key=guest').then(
    function(data){
      var x = data.replace(/\'/g, '"');
      var y  = JSON.parse(x);
      res.send(y);
  })
})

router.get('/cfb', function (req, res, next) {
  monthFinder();
  rp.get('http://api.sportsdatabase.com/ncaafb/query.JSON?sdql=line%2Ctotal%2Cpoints%2Cteam%2Co%3Ateam%2Co%3Apoints%2Csite%2Crest%2Co%3Arest%40date%3E20170108' + '&output=json&api_key=guest').then(function(data){
    var x = data.replace(/\'/g, '"');
    var y  = JSON.parse(x);
    res.send(y);
  })
})

module.exports = router;
