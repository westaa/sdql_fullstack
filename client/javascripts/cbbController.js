app.controller('cbbController', function ($rootScope, $scope, $http, $filter, sdqlService, $window, $location, $routeParams){

  $scope.view = {};
  $scope.view.date = sdqlService.date;
  $scope.view.selectedTeam = 'HT';
  $scope.view.selectedAwayTeam = '';

  $scope.view.reloadCBBPage = function () {
    $location.path('/CBB');
    $window.location.reload()
  }

  $scope.view.getCBBData = sdqlService.getCBBData().then(function(data){
    sdqlService.removeNullValues(data);
      $scope.view.CBBlines = sdqlService.filterData(data[0]);
      $scope.view.CBBATLines = [];
      for (var i = 0; i < $scope.view.CBBlines.length; i++) {
        if ($scope.view.CBBlines[i] == 'no data') {
          $scope.view.CBBATLines.push('no data')
        } else {
          $scope.view.CBBATLines.push($scope.view.CBBlines[i] * -1)
        }
      }
      $scope.view.CBBtotals = sdqlService.filterData(data[1]);
      $scope.view.CBBteams = sdqlService.filterData(data[3]);
      $scope.view.CBBopponents = sdqlService.filterData(data[4]);
      $scope.view.CBBsites = sdqlService.filterData(data[6]);
      $scope.view.CBBrest = sdqlService.filterData(data[7]);
      $scope.view.CBBoRest = sdqlService.filterData(data[8]);
    });


$scope.view.analyzeCBBMatchup = function (index) {
  var homeTeamKeyNumbers = sdqlService.getKeyNumbers($scope.view.CBBlines[index], $scope.view.CBBteams[index]);
  var awayTeamKeyNumbers = sdqlService.getKeyNumbers($scope.view.CBBATLines[index], $scope.view.CBBopponents[index]);
  var homeTeamRestArr = sdqlService.getRestDays($scope.view.CBBrest[index],  $scope.view.CBBteams[index]);
  var awayTeamRestArr = sdqlService.getRestDays($scope.view.CBBoRest[index], $scope.view.CBBopponents[index]);
  var homeTeamRest = $scope.view.CBBrest[index];
  var awayTeamRest = $scope.view.CBBoRest[index];
  $scope.view.selectedTeam = homeTeamKeyNumbers[0];
  $scope.view.selectedAwayTeam = awayTeamKeyNumbers[0];
  sdqlService.getCBBTeamData($scope.view.CBBteams[index]).then(function(data){
    sdqlService.removeNullValues(data);
    var dataCheck2 = sdqlService.getAvgSumTwoArr(data[2], data[5]);
    dataCheckAvg2 = Math.round(dataCheck2 * 100) / 100;
    $scope.view.dateHomeTeam = homeTeamKeyNumbers[0] + '\'s performance over the 2016-2017 season.'
    $scope.view.CBBHomeTeamLines = data[0];
    $scope.view.CBBHomeTeamDates = data[9];
    $scope.view.CBBHomeTeamTotals = data[1];
    $scope.view.CBBHomeTeamPoints = data[2];
    $scope.view.CBBHomeTeamOpponents = data[4];
    $scope.view.CBBHomeTeamoPoints = data[5];
    $scope.view.CBBHomeTeamSites = data[6];
    $scope.view.CBBHomeTeamRest = data[7];
    $scope.view.CBBHomeTeamoRest = data[8];
    $scope.view.CBBATSHomeTeamPerformance = [];
    $scope.view.CBBHomeTeamTotalPerformance = [];
    for (var i = 0; i < $scope.view.CBBHomeTeamLines.length; i++) {
      if ($scope.view.CBBHomeTeamLines[i] == 'no data'){
        $scope.view.CBBHomeTeamTotalPerformance.push('no data');
        $scope.view.CBBATSHomeTeamPerformance.push('no data')
      } else {
        $scope.view.CBBHomeTeamTotalPerformance.push($scope.view.CBBHomeTeamPoints[i] + $scope.view.CBBHomeTeamoPoints[i] - $scope.view.CBBHomeTeamTotals[i])
        $scope.view.CBBATSHomeTeamPerformance.push($scope.view.CBBHomeTeamPoints[i] - $scope.view.CBBHomeTeamoPoints[i] + $scope.view.CBBHomeTeamLines[i]);
      }
    }
    $scope.view.CBBHomeTeamAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSHomeTeamPerformance);
    $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamLines);
    $scope.view.CBBHomeTeamAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBHomeTeamPoints, $scope.view.CBBHomeTeamoPoints);
     $scope.view.homeTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamTotals);
     $scope.view.CBBaverageObj = {};
     $scope.view.CBBaverageObj.averagePointsScored = $scope.view.CBBHomeTeamAverageTotal;
     $scope.view.CBBaverageObj.averageOverUnder = $scope.view.homeTeamAverageOverUnder;
     $scope.view.CBBaverageObj.averagePointSpread = $scope.view.CBBHomeTeamAverageSpread;
     $scope.view.CBBaverageObj.averageATSPerformance = $scope.view.CBBHomeTeamAverageATSPerformance;
  }).then(function() {
    sdqlService.getCBBLastYearTeamData($scope.view.CBBteams[index]).then(function(data){
      data = sdqlService.removeNullObjValues(data);
      var dataCheck = sdqlService.getAvgSumTwoArr(data[2], data[5]);
      var dataCheckAvg = Math.round(dataCheck * 100) / 100;
      if (dataCheckAvg - 6.5 > dataCheckAvg2 || dataCheckAvg + 6.5 < dataCheckAvg2){
        dataYear = 2015;
        $scope.view.showHomeTeamLastYearData = false;
      } else {
        dataYear = 2014;
        $scope.view.showHomeTeamLastYearData = true;
        $scope.view.dateHomeTeamLastYear = homeTeamKeyNumbers[0] + '\'s performance over the 2015-2016 season.'
        $scope.view.CBBHomeTeamLastYearLines = data[0];
        $scope.view.CBBHomeTeamLastYearDates = data[9];
        $scope.view.CBBHomeTeamLastYearTotals = data[1];
        $scope.view.CBBHomeTeamLastYearPoints = data[2];
        $scope.view.CBBHomeTeamLastYearOpponents = data[4];
        $scope.view.CBBHomeTeamLastYearoPoints = data[5];
        $scope.view.CBBHomeTeamLastYearSites = data[6];
        $scope.view.CBBHomeTeamLastYearRest = data[7];
        $scope.view.CBBHomeTeamLastYearoRest = data[8];
        $scope.view.CBBATSHomeTeamLastYearPerformance = [];
        $scope.view.CBBHomeTeamLastYearTotalPerformance = [];
        for (var i = 0; i < $scope.view.CBBHomeTeamLastYearLines.length; i++) {
          if ($scope.view.CBBHomeTeamLastYearLines[i] == 'no data'){
            $scope.view.CBBHomeTeamLastYearTotalPerformance.push('no data');
            $scope.view.CBBATSHomeTeamLastYearPerformance.push('no data')
          } else {
            $scope.view.CBBHomeTeamLastYearTotalPerformance.push($scope.view.CBBHomeTeamLastYearPoints[i] + $scope.view.CBBHomeTeamLastYearoPoints[i] - $scope.view.CBBHomeTeamLastYearTotals[i])
            $scope.view.CBBATSHomeTeamLastYearPerformance.push($scope.view.CBBHomeTeamLastYearPoints[i] - $scope.view.CBBHomeTeamLastYearoPoints[i] + $scope.view.CBBHomeTeamLastYearLines[i]);
          }
        }
        $scope.view.CBBHomeTeamLastYearAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSHomeTeamLastYearPerformance);
        $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamLastYearLines);
        $scope.view.CBBawayTeamAverageTotal = dataCheckAvg;
        $scope.view.awayTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamLastYearTotals);
         $scope.view.CBBHomeTeamLastYearAverageObj = {};
         $scope.view.CBBHomeTeamLastYearAverageObj.averagePointsScored = $scope.view.CBBawayTeamAverageTotal;
         $scope.view.CBBHomeTeamLastYearAverageObj.averageOverUnder = $scope.view.awayTeamAverageOverUnder;
         $scope.view.CBBHomeTeamLastYearAverageObj.averagePointSpread = $scope.view.CBBHomeTeamAverageSpread;
         $scope.view.CBBHomeTeamLastYearAverageObj.averageATSPerformance = $scope.view.CBBHomeTeamLastYearAverageATSPerformance;
      }
    }).then(function () {
      var homeTeamKeyNumbers = sdqlService.getKeyNumbers($scope.view.CBBlines[index], $scope.view.CBBteams[index]);
      sdqlService.getCBBMatchupData(homeTeamKeyNumbers[1], homeTeamKeyNumbers[2], $scope.view.CBBteams[index], dataYear).then(function(data){
        data = sdqlService.removeNullObjValues(data);
        $scope.view.date = homeTeamKeyNumbers[0] + "'s performance since the start of the " + (dataYear+1).toString() + '-' + (dataYear+2).toString() + ' season with a point spread above ' + homeTeamKeyNumbers[1] + ' and a point spread below ' + homeTeamKeyNumbers[2] + '.';
        $scope.view.CBBlines = data.lines;
        $scope.view.CBBdates = data.dates;
        $scope.view.CBBtotals = data.totals;
        $scope.view.CBBpoints = data.points;
        $scope.view.CBBteams = data.teams;
        $scope.view.CBBopponents = data.opponents;
        $scope.view.CBBoPoints = data.oPoints;
        $scope.view.CBBsites = data.sites;
        $scope.view.CBBrest = data.rest;
        $scope.view.CBBoRest = data.oRest;
        $scope.view.ATSPerformance = [];
        $scope.view.CBBTotalPerformance = [];
        for (var i = 0; i < $scope.view.CBBpoints.length; i++) {
          $scope.view.ATSPerformance.push($scope.view.CBBpoints[i] - $scope.view.CBBoPoints[i] + $scope.view.CBBlines[i])
          $scope.view.CBBTotalPerformance.push($scope.view.CBBpoints[i] + $scope.view.CBBoPoints[i] - $scope.view.CBBtotals[i])
        }
        $scope.view.CBBHTeamAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.ATSPerformance);
        $scope.view.CBBHTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBlines);
        $scope.view.CBBHTeamAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBpoints, $scope.view.CBBoPoints);
        $scope.view.HTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBtotals);
         $scope.view.CBBHTeamAverageObj = {};
         $scope.view.CBBHTeamAverageObj.averagePointsScored = $scope.view.CBBHTeamAverageTotal;
         $scope.view.CBBHTeamAverageObj.averageOverUnder = $scope.view.HTeamAverageOverUnder;
         $scope.view.CBBHTeamAverageObj.averagePointSpread = $scope.view.CBBHTeamAverageSpread;
         $scope.view.CBBHTeamAverageObj.averageATSPerformance = $scope.view.CBBHTeamAverageATSPerformance;
      })
    }).then(function() {
        sdqlService.getCBBRestData(homeTeamRestArr[0], homeTeamRestArr[1], homeTeamRestArr[2], dataYear).then(function(data){
          data = sdqlService.removeNullValues(data);
          $scope.view.dateHomeTeamRestData = homeTeamRestArr[0] + '\'s performance since the start of the ' + (dataYear+1).toString() + '-' + (dataYear+2).toString() + ' season with ' + homeTeamRestArr[1] + ' or more days of rest and fewer than ' + homeTeamRestArr[2] + ' days of rest.';
          $scope.view.CBBHomeTeamRestDataLines = data[0];
          $scope.view.CBBHomeTeamRestDataDates = data[9];
          $scope.view.CBBHomeTeamRestDataTotals = data[1];
          $scope.view.CBBHomeTeamRestDataPoints = data[2];
          $scope.view.CBBHomeTeamRestDataOpponents = data[4];
          $scope.view.CBBHomeTeamRestDataoPoints = data[5];
          $scope.view.CBBHomeTeamRestDataSites = data[6];
          $scope.view.CBBHomeTeamRestDataRest = data[7];
          $scope.view.CBBHomeTeamRestDataoRest = data[8];
          $scope.view.CBBATSHomeTeamRestDataPerformance = [];
          $scope.view.CBBHomeTeamRestDataTotalPerformance = [];
          for (var i = 0; i < $scope.view.CBBHomeTeamRestDataLines.length; i++) {
            if ($scope.view.CBBHomeTeamRestDataLines[i] == 'no data'){
              $scope.view.CBBHomeTeamRestDataTotalPerformance.push('no data');
              $scope.view.CBBATSHomeTeamRestDataPerformance.push('no data')
            } else {
              $scope.view.CBBHomeTeamRestDataTotalPerformance.push($scope.view.CBBHomeTeamRestDataPoints[i] + $scope.view.CBBHomeTeamRestDataoPoints[i] - $scope.view.CBBHomeTeamRestDataTotals[i])
              $scope.view.CBBATSHomeTeamRestDataPerformance.push($scope.view.CBBHomeTeamRestDataPoints[i] - $scope.view.CBBHomeTeamRestDataoPoints[i] + $scope.view.CBBHomeTeamRestDataLines[i]);
            }
          }
          $scope.view.CBBHomeTeamRestDataAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSHomeTeamRestDataPerformance);
          $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamRestDataLines);
          $scope.view.CBBHomeTeamRestDataAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBHomeTeamRestDataPoints, $scope.view.CBBHomeTeamRestDataoPoints);
          $scope.view.CBBHomeTeamRestDataAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBHomeTeamRestDataTotals);
           $scope.view.CBBHomeTeamRestDataAverageObj = {};
           $scope.view.CBBHomeTeamRestDataAverageObj.averagePointsScored = $scope.view.CBBHomeTeamRestDataAverageTotal;
           $scope.view.CBBHomeTeamRestDataAverageObj.averageOverUnder = $scope.view.CBBHomeTeamRestDataAverageOverUnder;
           $scope.view.CBBHomeTeamRestDataAverageObj.averagePointSpread = $scope.view.CBBHomeTeamAverageSpread;
           $scope.view.CBBHomeTeamRestDataAverageObj.averageATSPerformance = $scope.view.CBBHomeTeamRestDataAverageATSPerformance;
        });
    }).then(function(){
      sdqlService.getCBBTeamData($scope.view.CBBopponents[index]).then(function(data){
        sdqlService.removeNullValues(data);
        var awayTeamDataCheck = sdqlService.getAvgSumTwoArr(data[2], data[5]);
        $scope.view.awayTeamDataCheckAvg = Math.round(awayTeamDataCheck * 100) / 100;
        $scope.view.dateAwayTeam = awayTeamKeyNumbers[0] + '\'s performance over the 2016-2017 season.'
        $scope.view.CBBAwayTeamLines = data[0];
        $scope.view.CBBAwayTeamDates = data[9];
        $scope.view.CBBAwayTeamTotals = data[1];
        $scope.view.CBBAwayTeamPoints = data[2];
        $scope.view.CBBAwayTeamOpponents = data[4];
        $scope.view.CBBAwayTeamoPoints = data[5];
        $scope.view.CBBAwayTeamSites = data[6];
        $scope.view.CBBAwayTeamRest = data[7];
        $scope.view.CBBAwayTeamoRest = data[8];
        $scope.view.CBBATSAwayTeamPerformance = [];
        $scope.view.CBBAwayTeamTotalPerformance = [];
        for (var i = 0; i < $scope.view.CBBAwayTeamLines.length; i++) {
          if ($scope.view.CBBAwayTeamLines[i] == 'no data'){
            $scope.view.CBBAwayTeamTotalPerformance.push('no data');
            $scope.view.CBBATSAwayTeamPerformance.push('no data')
          } else {
            $scope.view.CBBAwayTeamTotalPerformance.push($scope.view.CBBAwayTeamPoints[i] + $scope.view.CBBAwayTeamoPoints[i] - $scope.view.CBBAwayTeamTotals[i])
            $scope.view.CBBATSAwayTeamPerformance.push($scope.view.CBBAwayTeamPoints[i] - $scope.view.CBBAwayTeamoPoints[i] + $scope.view.CBBAwayTeamLines[i]);
          }
        }
        $scope.view.CBBAwayTeamAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSAwayTeamPerformance);
        $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamLines);
        $scope.view.CBBawayTeamAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBAwayTeamPoints, $scope.view.CBBAwayTeamoPoints);
        $scope.view.awayTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamTotals);
         $scope.view.CBBAwayTeamAverageObj = {};
         $scope.view.CBBAwayTeamAverageObj.averagePointsScored = $scope.view.CBBawayTeamAverageTotal;
         $scope.view.CBBAwayTeamAverageObj.averageOverUnder = $scope.view.awayTeamAverageOverUnder;
         $scope.view.CBBAwayTeamAverageObj.averagePointSpread = $scope.view.CBBHomeTeamAverageSpread;
         $scope.view.CBBAwayTeamAverageObj.averageATSPerformance = $scope.view.CBBAwayTeamAverageATSPerformance;
      })
    }).then(function(awayTeamDataCheckAvg = $scope.view.awayTeamDataCheckAvg){
        sdqlService.getCBBLastYearTeamData($scope.view.CBBopponents[index]).then(function(data){
          data = sdqlService.removeNullObjValues(data);
          var awayTeamDataCheck2 = sdqlService.getAvgSumTwoArr(data[2], data[5]);
          var awayTeamDataCheckAvg2 = Math.round(awayTeamDataCheck2 * 100) / 100;
          if (awayTeamDataCheckAvg - 6.5 > awayTeamDataCheckAvg2 || awayTeamDataCheckAvg + 6.5 < awayTeamDataCheckAvg2){
            $scope.view.awayTeamDataYear = 2014;
            $scope.view.showAwayTeamLastYearData = true;
            $scope.view.dateAwayTeamLastYear = awayTeamKeyNumbers[0] + '\'s performance over the 2015-2016 season.'
            $scope.view.CBBAwayTeamLastYearLines = data[0];
            $scope.view.CBBAwayTeamLastYearDates = data[9];
            $scope.view.CBBAwayTeamLastYearTotals = data[1];
            $scope.view.CBBAwayTeamLastYearPoints = data[2];
            $scope.view.CBBAwayTeamLastYearOpponents = data[4];
            $scope.view.CBBAwayTeamLastYearoPoints = data[5];
            $scope.view.CBBAwayTeamLastYearSites = data[6];
            $scope.view.CBBAwayTeamLastYearRest = data[7];
            $scope.view.CBBAwayTeamLastYearoRest = data[8];
            $scope.view.CBBATSAwayTeamLastYearPerformance = [];
            $scope.view.CBBAwayTeamLastYearTotalPerformance = [];
            for (var i = 0; i < $scope.view.CBBAwayTeamLastYearLines.length; i++) {
              if ($scope.view.CBBAwayTeamLastYearLines[i] == 'no data'){
                $scope.view.CBBAwayTeamLastYearTotalPerformance.push('no data');
                $scope.view.CBBATSAwayTeamLastYearPerformance.push('no data')
              } else {
                $scope.view.CBBAwayTeamLastYearTotalPerformance.push($scope.view.CBBAwayTeamLastYearPoints[i] + $scope.view.CBBAwayTeamLastYearoPoints[i] - $scope.view.CBBAwayTeamLastYearTotals[i])
                $scope.view.CBBATSAwayTeamLastYearPerformance.push($scope.view.CBBAwayTeamLastYearPoints[i] - $scope.view.CBBAwayTeamLastYearoPoints[i] + $scope.view.CBBAwayTeamLastYearLines[i]);
              }
            }
            $scope.view.CBBAwayTeamLastYearAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSAwayTeamLastYearPerformance);
            $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamLastYearLines);
            $scope.view.CBBawayTeamAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBAwayTeamLastYearPoints, $scope.view.CBBAwayTeamLastYearoPoints);
            $scope.view.awayTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamLastYearTotals);
             $scope.view.CBBAwayTeamLastYearAverageObj = {};
             $scope.view.CBBAwayTeamLastYearAverageObj.averagePointsScored = $scope.view.CBBawayTeamAverageTotal;
             $scope.view.CBBAwayTeamLastYearAverageObj.averageOverUnder = $scope.view.awayTeamAverageOverUnder;
             $scope.view.CBBAwayTeamLastYearAverageObj.averagePointSpread = $scope.view.CBBHomeTeamAverageSpread;
             $scope.view.CBBAwayTeamLastYearAverageObj.averageATSPerformance = $scope.view.CBBAwayTeamLastYearAverageATSPerformance;
          } else {
            $scope.view.awayTeamDataYear = 2015;
            $scope.view.showAwayTeamLastYearData = false;
           }
         }).then(function(awayTeamDataYear = $scope.view.awayTeamDataYear){
      sdqlService.getCBBMatchupData(awayTeamKeyNumbers[1], awayTeamKeyNumbers[2], awayTeamKeyNumbers[0], awayTeamDataYear).then(function(data) {
        data = sdqlService.removeNullObjValues(data);
        $scope.view.dateATeam = awayTeamKeyNumbers[0] + "'s performance since the start of the " + (awayTeamDataYear+1).toString() + '-' + (awayTeamDataYear+2).toString() + ' season with a point spread above ' + awayTeamKeyNumbers[1] + ' and a point spread below ' + awayTeamKeyNumbers[2] + '.';
        $scope.view.CBBATeamLines = data.lines;
        $scope.view.CBBATeamDates = data.dates;
        $scope.view.CBBATeamTotals = data.totals;
        $scope.view.CBBATeamPoints = data.points;
        $scope.view.CBBATeamTeams = data.teams;
        $scope.view.CBBATeamOpponents = data.opponents;
        $scope.view.CBBATeamoPoints = data.oPoints;
        $scope.view.CBBATeamSites = data.sites;
        $scope.view.CBBATeamRest = data.rest;
        $scope.view.CBBATeamoRest = data.oRest;
        $scope.view.CBBATSATeamPerformance = [];
        $scope.view.CBBATeamTotalPerformance = [];
        for (var i = 0; i < $scope.view.CBBATeamPoints.length; i++) {
          $scope.view.CBBATSATeamPerformance.push($scope.view.CBBATeamPoints[i] - $scope.view.CBBATeamoPoints[i] + $scope.view.CBBATeamLines[i])
          $scope.view.CBBATeamTotalPerformance.push($scope.view.CBBATeamPoints[i] + $scope.view.CBBATeamoPoints[i] - $scope.view.CBBATeamTotals[i])
        }
        $scope.view.CBBATeamAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSATeamPerformance);
        $scope.view.CBBATeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBATeamLines);
        $scope.view.CBBawayTeamAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBATeamPoints, $scope.view.CBBATeamoPoints);
        $scope.view.awayTeamAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBATeamTotals);
         $scope.view.CBBATeamAverageObj = {};
         $scope.view.CBBATeamAverageObj.averagePointsScored = $scope.view.CBBawayTeamAverageTotal;
         $scope.view.CBBATeamAverageObj.averageOverUnder = $scope.view.awayTeamAverageOverUnder;
         $scope.view.CBBATeamAverageObj.averagePointSpread = $scope.view.CBBATeamAverageSpread;
         $scope.view.CBBATeamAverageObj.averageATSPerformance = $scope.view.CBBATeamAverageATSPerformance;
      })
    }).then(function(awayTeamDataYear = $scope.view.awayTeamDataYear) {
            sdqlService.getCBBRestData(awayTeamRestArr[0], awayTeamRestArr[1], awayTeamRestArr[2], awayTeamDataYear).then(function(data){
              data = sdqlService.removeNullValues(data);
              $scope.view.dateAwayTeamRestData = awayTeamRestArr[0] + '\'s performance since the start of the ' + (awayTeamDataYear+1).toString() + '-' + (awayTeamDataYear+2).toString() + ' season with ' + awayTeamRestArr[1] + ' or more days of rest and fewer than ' + awayTeamRestArr[2] + ' days of rest.';
              $scope.view.CBBAwayTeamRestDataLines = data[0];
              $scope.view.CBBAwayTeamRestDataDates = data[9];
              $scope.view.CBBAwayTeamRestDataTotals = data[1];
              $scope.view.CBBAwayTeamRestDataPoints = data[2];
              $scope.view.CBBAwayTeamRestDataOpponents = data[4];
              $scope.view.CBBAwayTeamRestDataoPoints = data[5];
              $scope.view.CBBAwayTeamRestDataSites = data[6];
              $scope.view.CBBAwayTeamRestDataRest = data[7];
              $scope.view.CBBAwayTeamRestDataoRest = data[8];
              $scope.view.CBBATSAwayTeamRestDataPerformance = [];
              $scope.view.CBBAwayTeamRestDataTotalPerformance = [];
              for (var i = 0; i < $scope.view.CBBAwayTeamRestDataLines.length; i++) {
                if ($scope.view.CBBAwayTeamRestDataLines[i] == 'no data'){
                  $scope.view.CBBAwayTeamRestDataTotalPerformance.push('no data');
                  $scope.view.CBBATSAwayTeamRestDataPerformance.push('no data')
                } else {
                  $scope.view.CBBAwayTeamRestDataTotalPerformance.push($scope.view.CBBAwayTeamRestDataPoints[i] + $scope.view.CBBAwayTeamRestDataoPoints[i] - $scope.view.CBBAwayTeamRestDataTotals[i])
                  $scope.view.CBBATSAwayTeamRestDataPerformance.push($scope.view.CBBAwayTeamRestDataPoints[i] - $scope.view.CBBAwayTeamRestDataoPoints[i] + $scope.view.CBBAwayTeamRestDataLines[i]);
                }
              }
              $scope.view.CBBAwayTeamRestDataAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSAwayTeamRestDataPerformance);
              $scope.view.CBBHomeTeamAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamRestDataLines);
              $scope.view.CBBAwayTeamRestDataAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBAwayTeamRestDataPoints, $scope.view.CBBAwayTeamRestDataoPoints);
              $scope.view.CBBAwayTeamRestDataAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBAwayTeamRestDataTotals);
               $scope.view.CBBAwayTeamRestDataAverageObj = {};
               $scope.view.CBBAwayTeamRestDataAverageObj.averagePointsScored = $scope.view.CBBAwayTeamRestDataAverageTotal;
               $scope.view.CBBAwayTeamRestDataAverageObj.averageOverUnder = $scope.view.CBBAwayTeamRestDataAverageOverUnder;
               $scope.view.CBBAwayTeamRestDataAverageObj.averagePointSpread = $scope.view.CBBAwayTeamAverageSpread;
               $scope.view.CBBAwayTeamRestDataAverageObj.averageATSPerformance = $scope.view.CBBAwayTeamRestDataAverageATSPerformance;
            });
          })
        }).then(function() {
          sdqlService.getCBBVersusData($scope.view.CBBteams[index], $scope.view.CBBopponents[index]).then(function(data){
          if (data[0].length > 0) {
          $scope.view.CBBVersusHomeTeam = homeTeamKeyNumbers[0];
          $scope.view.CBBVersusAwayTeam = awayTeamKeyNumbers[0];
          data = sdqlService.removeNullValues(data);
          $scope.view.dateVersus = homeTeamKeyNumbers[0] + '\'s performance since the start of the 2015-2016 season versus ' + awayTeamKeyNumbers[0];
          $scope.view.CBBVersusLines = data[0];
        $scope.view.CBBVersusDates = data[9];
        $scope.view.CBBVersusTotals = data[1];
        $scope.view.CBBVersusPoints = data[2];
        $scope.view.CBBVersusOpponents = data[4];
        $scope.view.CBBVersusoPoints = data[5];
        $scope.view.CBBVersusSites = data[6];
        $scope.view.CBBVersusRest = data[7];
        $scope.view.CBBVersusoRest = data[8];
        $scope.view.CBBATSVersusPerformance = [];
        $scope.view.CBBVersusTotalPerformance = [];
        for (var i = 0; i < $scope.view.CBBVersusLines.length; i++) {
          if ($scope.view.CBBVersusLines[i] == 'no data'){
            $scope.view.CBBVersusTotalPerformance.push('no data');
            $scope.view.CBBATSVersusPerformance.push('no data')
          } else {
            $scope.view.CBBVersusTotalPerformance.push($scope.view.CBBVersusPoints[i] + $scope.view.CBBVersusoPoints[i] - $scope.view.CBBVersusTotals[i])
            $scope.view.CBBATSVersusPerformance.push($scope.view.CBBVersusPoints[i] - $scope.view.CBBVersusoPoints[i] + $scope.view.CBBVersusLines[i]);
          }
        }


        $scope.view.CBBVersusAverageATSPerformance = sdqlService.getAvgSingleArr($scope.view.CBBATSVersusPerformance);
        $scope.view.CBBVersusAverageSpread = sdqlService.getAvgSingleArr($scope.view.CBBVersusLines);
        $scope.view.CBBVersusAverageTotal = sdqlService.getAvgSumTwoArr($scope.view.CBBVersusPoints, $scope.view.CBBVersusoPoints);
        $scope.view.CBBVersusAverageOverUnder = sdqlService.getAvgSingleArr($scope.view.CBBVersusTotals);
       $scope.view.CBBVersusAverageObj = {};
       $scope.view.CBBVersusAverageObj.averagePointsScored = $scope.view.CBBVersusAverageTotal;
       $scope.view.CBBVersusAverageObj.averageOverUnder = $scope.view.CBBVersusAverageOverUnder;
       $scope.view.CBBVersusAverageObj.averagePointSpread = $scope.view.CBBVersusAverageSpread;
       $scope.view.CBBVersusAverageObj.averageATSPerformance = $scope.view.CBBVersusAverageATSPerformance;
      }
    })
  })
  })
}

});
