app.controller('sdqlController', function($rootScope, $scope, $http, $filter, sdqlService, $window, $location) {

  $scope.view = {};
  $scope.view.info = {};
  $scope.view.greeting = "Trend-based Sports Analytics";
  $scope.view.successGreeting = sdqlService.successGreeting;
  $scope.view.date = sdqlService.date;

});

app.controller('nbaController', function($rootScope, $scope, $http, $filter, sdqlService, $window, $location){
  $scope.view = {};
  $scope.view.date = sdqlService.date;
  $scope.view.getNBAData = sdqlService.getNBAData().then(function(data){
    sdqlService.removeNullNBAValues(data);
    $scope.view.NBAlines = sdqlService.filterData(data[0]);
    $scope.view.NBAtotals = sdqlService.filterData(data[1]);
    $scope.view.NBApoints = sdqlService.filterData(data[2]);
    $scope.view.NBAteams = sdqlService.filterData(data[3]);
    $scope.view.NBAopponents = sdqlService.filterData(data[4]);
    $scope.view.NBAoPoints = sdqlService.filterData(data[5]);
    $scope.view.NBAsites = sdqlService.filterData(data[6]);
    $scope.view.NBArest = sdqlService.filterData(data[7]);
    $scope.view.NBAoRest = sdqlService.filterData(data[8]);
    sdqlService.removeNullValues(data);
    sdqlService.fillGreeting();
  });
});

app.controller('nflController', function ( $rootScope, $scope, $http, $filter, sdqlService, $window, $location){

  $scope.view.date = sdqlService.date;
  $scope.view.getNFLdata =  sdqlService.getNFLData().then(function(data){
    sdqlService.removeNullValues(data);
    sdqlService.fillGreeting()
    $scope.view.NFLlines = sdqlService.filterData(data.data.groups[0].columns[0]);
    $scope.view.NFLtotals = sdqlService.filterData(data.data.groups[0].columns[1]);
    $scope.view.NFLpoints = sdqlService.filterData(data.data.groups[0].columns[2]);
    $scope.view.NFLteams = sdqlService.filterData(data.data.groups[0].columns[3]);
    $scope.view.NFLopponents = sdqlService.filterData(data.data.groups[0].columns[4]);
    $scope.view.NFLoPoints = sdqlService.filterData(data.data.groups[0].columns[5]);
    $scope.view.NFLsites = sdqlService.filterData(data.data.groups[0].columns[6]);
    $scope.view.NFLrest = sdqlService.filterData(data.data.groups[0].columns[7]);
    $scope.view.NFLoRest = sdqlService.filterData(data.data.groups[0].columns[8]);
  });

});

app.controller('cfbController', function ($rootScope, $scope, $http, $filter, sdqlService, $window, $location) {
  $scope.view.date = sdqlService.date;
  $scope.view.getCFBData =
   sdqlService.getCFBData().then(function(data){
      sdqlService.removeNullValues(data);
      sdqlService.fillGreeting()
      $scope.view.CFBlines = sdqlService.filterData(data.data.groups[0].columns[0]);
      $scope.view.CFBtotals = sdqlService.filterData(data.data.groups[0].columns[1]);
      $scope.view.CFBpoints = sdqlService.filterData(data.data.groups[0].columns[2]);
      $scope.view.CFBteams = sdqlService.filterData(data.data.groups[0].columns[3]);
      $scope.view.CFBopponents = sdqlService.filterData(data.data.groups[0].columns[4]);
      $scope.view.CFBoPoints = sdqlService.filterData(data.data.groups[0].columns[5]);
      $scope.view.CFBsites = sdqlService.filterData(data.data.groups[0].columns[6]);
      $scope.view.CFBrest = sdqlService.filterData(data.data.groups[0].columns[7]);
      $scope.view.CFBoRest = sdqlService.filterData(data.data.groups[0].columns[8]);
  });
});




// http://api.sportsdatabase.com/nfl/query.json?sdql=date%2Cpoints%40team%3DBears%20and%20season%3D2011%output=json%api_key=guest
