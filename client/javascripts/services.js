app.factory('sdqlService', function ($http, $window, $location) {
  var sdqlService = this;
  return {
    getCBBData: function () {
      return $http({
        method: 'GET',
        dataType: 'json',
        url: '/api/cbb/'
      }).then(function(data) {
        data = data.data.groups[0].columns;
        return data;
      })
    },
    getCBBMatchupData: function (line, line2, team, year) {
      return $http ({
        method: 'GET',
        dataType: 'json',
        url: '/api/cbb/lineData/' + line + '/' + line2 + '/' + team + '/' + year + '/'
      }).then(function(data){
      if (data.data === "") {
        data = {
          lines: ['no data', 'no data', 'no data'], dates: ['no data', 'no data', 'no data'], totals: ['no data', 'no data', 'no data'], points: ['no data', 'no data', 'no data'], opponents: ['no data', 'no data', 'no data'], oPoints: ['no data', 'no data', 'no data'], sites: ['no data', 'no data', 'no data'], rest: ['no data', 'no data', 'no data'], oRest: ['no data', 'no data', 'no data']
        }
      } else if (data) {
        data = data.data.groups[0].columns;
        data = {
          lines: data[0], dates: data[9], totals: data[1], points: data[2], opponents: data[4], oPoints: data[5], sites: data[6], rest: data[7], oRest: data[8]
        };
      }
      return data;
      })
    },
    getCBBTeamData: function (team) {
      return $http ({
        method: 'GET',
        Type: 'json',
        url: '/api/cbb/teamData/' + team + '/'
      }).then(function(data){
        data = data.data.groups[0].columns;
        return data
      }).catch(function(error){
      })
    },
    getCBBLastYearTeamData: function (team) {
      return $http ({
        method: 'GET',
        Type: 'json',
        url: '/api/cbb/lastYearTeamData/' + team + '/'
      }).then(function(data) {
        data = data.data.groups[0].columns;
        return data;
      })
    },
    getCBBRestData: function (team, line1, line2, year) {
      return $http ({
        method: 'GET',
        type: 'json',
        url: '/api/cbb/restData/' + team + '/' + line1 + '/' + line2 + '/' + year + '/'
       }).then(function(data) {
        data = data.data.groups[0].columns;
        return data;
      })
    },
    getCBBVersusData: function (team, oTeam) {
      return $http ({
        method: 'GET',
        type: 'json',
        url: '/api/cbb/versusData/' + team + '/' + oTeam + '/'
      }).then(function(data){
        data = data.data.groups[0].columns;
        return data;
      }).catch(function(error) {
        console.log(error);
      })
    },
    getNBAData: function () {
      return $http({
        method: 'GET',
        dataType: 'json',
        url: '/api/nba/'
      }).then(function(data){
        data = data.data.groups[0].columns;
        return data;
      })
    },
    getNFLData: function () {
      return $http({
        method: 'GET',
        dataType: 'json',
        url: '/api/nfl/'
      })
    },
    getCFBData: function () {
      return $http({
        method: 'GET',
        dataType: 'json',
        url: '/api/cfb/'
      })
    },
    filterData: function (inputArr) {
        return inputArr.filter(function(elem, index){
          if (index % 2 === 0) {
            return elem
          }
        })
      },
    removeNullValues:  function (data) {
      for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < data[i].length; j++) {
          if(data[i][j] === null) {
            data[i][j] = 'no data'
          }
          if(data[i][j] === 0) {
            data[i][j] = '0';
          }
        }
      }
      return data
    },
    removeNullObjValues: function(data) {
      for (var x in  data) {
        for (var i = 0; i < data[x].length; i++) {
          if (data[x][i] === null || data[x][i] === 0) {
            data[x][i] = 'no data'
          }
          if (data[x][i] == 0) {
            data[x][i] = '0';
          }
        }
      }
      return data
    },
    removeNullNBAValues: function (data) {
      for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < data[i].length; j++) {
          if(data[i][j] === null) {
            data[i][j] = 'no data'
          } else if (data[i][j] === 0) {
            data[i][j] = '0';
          };
        }
      }
      return data
    },
    getAvgSingleArr (arr) {
      if (typeof arr !== 'object'){
        return 'no data';
      }
      var arr = arr;
      var avg = Math.round(arr.filter(
        function(elem, ind) {
        return elem !== 'no data';
       }).reduce(function(a,b){
         return a + b;
       })/arr.filter(function(elem, ind) {
         return elem !== 'no data';
       }).length * 100) / 100;
       return avg;
    },
    getAvgSumTwoArr (arr, arr2) {
      if (typeof arr !== 'object'){
        return 'no data';
      }
      var arr = arr;
      var arr2 = arr2;
      var avg = Math.round((arr.filter(function(elem, ind) {
        return elem != 'no data'
      }).reduce(function(a,b) {
         return a + b
       }) + arr2.filter(function(elem, ind){
         return elem != 'no data'
       }).reduce(function(a,b) {
         return a + b
       }))/arr.filter(function(elem, ind) {
         return elem != 'no data'
       }).length * 100) / 100;
       return avg;
    },
    getKeyNumbers: function (line, team) {
      var keyNumberSearch = [];
      keyNumberSearch[0] = team;
      var keyNumbers = [-35, -20, -7.5, 0, 7.5, 20, 35];
      for (var i = 0; i < keyNumbers.length; i++) {
        if (line == keyNumbers[i] && line) {
          keyNumberSearch.push(keyNumbers[i - 1], keyNumbers[i + 1])
        } else if (line < keyNumbers[0]) {
          keyNumberSearch[0] = -100;
          keyNumberSearch[1] = keyNumbers[0];
        } else if (line > keyNumbers[i] && line < keyNumbers[i + 1]) {
          keyNumberSearch.push(keyNumbers[i], keyNumbers[i + 1]);
        } else if (line > keyNumbers[keyNumbers.length -1]) {
          keyNumberSearch[0] = keyNumbers[keyNumbers.length -1];
          keyNumberSearch[1] = 100;
        }
      }
      return keyNumberSearch;
      },
      getRestDays: function (rest, team) {
        var restDaysArr = [];
        restDaysArr[0] = team;
        var restArr = [0, 3, 5, 35];
        for (var i = 0; i < restArr.length; i++) {
          if (rest >= restArr[i] && rest < restArr[i + 1]) {
            restDaysArr.push(restArr[i], restArr[i + 1])
          }
        }
        return restDaysArr;
      },
    date: 'Games for ' + window.Date().toString().split(' ')[0] + ', ' + window.Date().toString().split(' ')[1] + '. ' + window.Date().toString().split(' ')[2] + ', ' + window.Date().toString().split(' ')[3],
    };

});
