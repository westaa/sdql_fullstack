var app = angular.module('SDQL', ['ngRoute', 'ngResource']);

app.config(function($routeProvider){
  $routeProvider

    .when('/', {
      templateUrl: "./partials/landing.html",
      controller: 'sdqlController'
    })

    .when('/CBB', {
      templateUrl: "./partials/cbb/cbb.html",
      controller: 'cbbController'
    })

    .when('/CBB/getData', {
      templateUrl: "./partials/cbb/analysis.html",
      controller: 'cbbController'
    })

    .when('/NBA', {
      templateUrl: './partials/nba/nba.html',
      controller: 'nbaController'
    })

    .when('/NFL', {
      templateUrl: './partials/nfl/nfl.html',
      controller: 'nflController'
    })

    .when('/CFB', {
      templateUrl: './partials/cfb/cfb.html',
      controller: 'cfbController'
    })

})
